(function () {
    'use strict';

    $(window).on('load', function () {
        setTimeout(() => {
            $('body').addClass('loaded');
        }, 500)
    });

    $(document).ready(function () {
        const $btnTogglePassword = $('#btnTogglePassword');
        const $form = $('form');
        const $password = $form.find('.form__control input[type=password]');
        const $email = $form.find('.form__control input[type=email]');

        $btnTogglePassword.on('click', function () {
            let $this = $(this);

            $this.toggleClass('show');

            if ($this.hasClass('show')) {
                $this.siblings('input[type=password]').attr('type', 'text');
            } else {
                $this.siblings('input[type=text]').attr('type', 'password');
            }
        });

        $form.on('submit', function (event) {
            event.preventDefault();

            removeValidation.call(this);
            checkFieldsPresence();
            checkPasswordMatch();
            checkValidateEmail();
        });

        $password.on('input', function () {
            removeValidation.call(this);
            checkFieldPresence.call(this);
            checkPasswordMatch()
        });

        $email.on('input', function () {
            removeValidation.call(this);
            checkFieldPresence.call(this);
            checkValidateEmail()
        });

        function removeValidation() {
            let errors;

            if ($(this).is('form')) {
                errors = $(this).find('.text-error');
            }

            if ($(this).is('input')) {
                errors = $(this).closest('.form__group').find('.text-error');
            }

            if (errors) {
                errors.each((index, error) => {
                    $(error).remove();
                });
            }
        }

        function generateError(text) {
            const $error = $('<div></div>');

            return $error.addClass('text-error').text(text);
        }

        function checkFieldsPresence() {
            $('input[required]').each((index, field) => {
                if (!$(field).val()) {
                    let error = generateError('Cant be blank');
                    $(error).insertAfter($(field).closest('.form__control'));
                }
            })
        }

        function checkFieldPresence() {
            if (!$(this).val()) {
                let error = generateError('Cant be blank');
                $(error).insertAfter($(this).closest('.form__control'));
            }
        }

        function checkPasswordMatch() {
            const $parent = $password.closest('.form__control');

            if ($password.val().length < 8) {
                let error = generateError('Password doesn\'t match');

                $(error).insertAfter($parent);
                $parent.removeClass('valid');
                $parent.addClass('invalid');
            } else {
                $parent.removeClass('invalid');
                $parent.addClass('valid');
            }
        }

        function checkValidateEmail() {
            let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            const $parent = $email.closest('.form__control');

            if (!re.test($email.val().toLowerCase())) {
                let error = generateError('Email incorrect');

                $(error).insertAfter($parent);
                $parent.removeClass('valid');
                $parent.addClass('invalid');
            } else {
                $parent.removeClass('invalid');
                $parent.addClass('valid');
            }
        }

    });

})();